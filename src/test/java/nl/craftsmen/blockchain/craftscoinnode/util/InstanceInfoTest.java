package nl.craftsmen.blockchain.craftscoinnode.util;

import nl.craftsmen.blockchain.craftscoinnode.CraftsCoinConfigurationProperties;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.web.ServerProperties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InstanceInfoTest {


    private InstanceInfo instanceInfo;
    private ServerProperties serverProperties;

    @Before
    public void setup() {
        serverProperties = mock(ServerProperties.class);
        when(serverProperties.getPort()).thenReturn(8080);
    }
    @Test
    public void nonConfiguredIpShouldResultInAutomaticIpDiscovery() {
        instanceInfo = new InstanceInfo(new CraftsCoinConfigurationProperties());
        instanceInfo.init();
        assertThat(instanceInfo.getPort()).isEqualTo(0);
     }

    @Test
    public void configuredIpShouldProduceThatIpForThisNode() {
        CraftsCoinConfigurationProperties craftsCoinConfigurationProperties = new CraftsCoinConfigurationProperties();
        craftsCoinConfigurationProperties.setIpAddress("192.168.0.170");
        instanceInfo = new InstanceInfo(craftsCoinConfigurationProperties);
        instanceInfo.init();
        assertThat(instanceInfo.getPort()).isEqualTo(0);
        instanceInfo.setPort(8080);
        assertThat(instanceInfo.getNode()).isEqualTo("192.168.0.170:8080");
    }
}