package nl.craftsmen.blockchain.craftscoinnode.network;

import nl.craftsmen.blockchain.craftscoinnode.CraftsCoinConfigurationProperties;
import nl.craftsmen.blockchain.craftscoinnode.blockchain.Block;
import nl.craftsmen.blockchain.craftscoinnode.blockchain.Blockchain;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;
import nl.craftsmen.blockchain.craftscoinnode.util.GenericRepository;
import nl.craftsmen.blockchain.craftscoinnode.util.InstanceInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NetworkTest {

    @Mock
    private GenericRepository genericRepository;

    @Mock
    private InstanceInfo instanceInfo;

    @Mock
    private RestTemplate restTemplate;

    @Captor
    private ArgumentCaptor<Peers> peersCaptor;

    @Captor
    private ArgumentCaptor<HttpEntity<Block>> blockHttpEntityCaptor;

    private Network network;

    @Before
    public void setup() {
        when(instanceInfo.getNode()).thenReturn("localhost:8080");
        when(instanceInfo.getPort()).thenReturn(8080);
        CraftsCoinConfigurationProperties craftsCoinConfigurationProperties = new CraftsCoinConfigurationProperties();
        craftsCoinConfigurationProperties.setBootstrapPeerHost("localhost");
        craftsCoinConfigurationProperties.setBootstrapPeerPort(8080);
        network = new Network(instanceInfo, genericRepository, restTemplate, craftsCoinConfigurationProperties);
    }

    @Test
    public void connectingToNetworkWithNoKnownPeersShouldRegisterThroughTheBootstrapPeer() {
        CraftsCoinConfigurationProperties craftsCoinConfigurationProperties = new CraftsCoinConfigurationProperties();
        craftsCoinConfigurationProperties.setBootstrapPeerHost("localhost");
        craftsCoinConfigurationProperties.setBootstrapPeerPort(9000);
        network = new Network(instanceInfo, genericRepository, restTemplate, craftsCoinConfigurationProperties);
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(new Peers()));
        Set<String> remotePeers = Stream.of("newRemotePeer:8080").collect(Collectors.toSet());
        ResponseEntity<Set<String>> response = new ResponseEntity<>(remotePeers, HttpStatus.OK);
        when(restTemplate.exchange(eq("http://localhost:9000/api/registernode"), eq(HttpMethod.POST), ArgumentMatchers.<HttpEntity<Object>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<Set<String>>>any())).thenReturn(response);
        network.init();
        network.connectToNetwork();
        verify(genericRepository).save(peersCaptor.capture());
        assertThat(network.getPeers()).containsExactlyInAnyOrder("newRemotePeer:8080");
        assertThat(peersCaptor.getValue().getPeers()).containsExactlyInAnyOrder("newRemotePeer:8080");
    }

    @Test
    public void connectingToNetworkWithAKnownPeerShouldAddAllNewPeersToThePeerList() {
        Peers peers = new Peers();
        peers.setPeers(Stream.of("alreadyKnownPeer:8080").collect(Collectors.toSet()));
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(peers));
        Set<String> remotePeers = Stream.of("newRemotePeer:8080").collect(Collectors.toSet());
        ResponseEntity<Set<String>> response = new ResponseEntity<>(remotePeers, HttpStatus.OK);
        when(restTemplate.exchange(eq("http://alreadyKnownPeer:8080/api/registernode"), eq(HttpMethod.POST), ArgumentMatchers.<HttpEntity<Object>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<Set<String>>>any())).thenReturn(response);
        network.init();
        network.connectToNetwork();
        verify(genericRepository).save(peersCaptor.capture());
        assertThat(network.getPeers()).containsExactlyInAnyOrder("newRemotePeer:8080", "alreadyKnownPeer:8080");
        assertThat(peersCaptor.getValue().getPeers()).containsExactlyInAnyOrder("newRemotePeer:8080", "alreadyKnownPeer:8080");
    }

    @Test
    public void connectingToNetworkWithTwoKnownPeersOfWhichOneIsDownAddAllNewPeersToThePeerListAndRemoveTheBrokenNode() {
        Peers peers = new Peers();
        peers.setPeers((Stream.of("alreadyKnownPeer1:8080", "alreadyKnownPeer2:8080").collect(Collectors.toSet())));
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(peers));
        Set<String> remotePeers = Stream.of("newRemotePeer:8080").collect(Collectors.toSet());
        ResponseEntity<Set<String>> response = new ResponseEntity<>(remotePeers, HttpStatus.OK);
        when(restTemplate.exchange(eq("http://alreadyKnownPeer1:8080/api/registernode"), eq(HttpMethod.POST), ArgumentMatchers.<HttpEntity<Object>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<Set<String>>>any())).thenThrow(new ResourceAccessException("connection refused"));
        when(restTemplate.exchange(eq("http://alreadyKnownPeer2:8080/api/registernode"), eq(HttpMethod.POST), ArgumentMatchers.<HttpEntity<Object>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<Set<String>>>any())).thenReturn(response);
        network.init();
        network.connectToNetwork();
        assertThat(network.getPeers()).containsExactlyInAnyOrder("newRemotePeer:8080", "alreadyKnownPeer2:8080");
    }

    @Test
    public void peerIsRegisteredAndPropagatedToOtherPeers() {
        Peers peers = new Peers();
        peers.setPeers(Stream.of("alreadyKnownPeer:8080").collect(Collectors.toSet()));
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(peers));
        Set<String> remotePeers = Stream.of("newRemotePeer:8080").collect(Collectors.toSet());
        ResponseEntity<Set<String>> response = new ResponseEntity<>(remotePeers, HttpStatus.OK);
        when(restTemplate.exchange(eq("http://alreadyKnownPeer:8080/api/registernode"), eq(HttpMethod.POST), ArgumentMatchers.<HttpEntity<Object>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<Set<String>>>any())).thenReturn(response);
        String newPeer = "newPeer:8080";
        network.init();
        Set<String> peersForRemote = network.registerNewPeer(newPeer);
        verify(genericRepository).save(peersCaptor.capture());
        assertThat(network.getPeers()).containsExactlyInAnyOrder("newRemotePeer:8080", "alreadyKnownPeer:8080", newPeer);
        assertThat(peersCaptor.getValue().getPeers()).containsExactlyInAnyOrder("newRemotePeer:8080", "alreadyKnownPeer:8080", newPeer);
        assertThat(peersForRemote).doesNotContain(newPeer);
    }

    @Test
    public void notifyPeerOfNewTransactionShouldPropagateTheTransactionToThePeers() {
        Peers peers = new Peers();
        peers.setPeers(Stream.of("alreadyKnownPeer:8080").collect(Collectors.toSet()));
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(peers));
        String sourcePeer = "sourcePeer:8080";
        Transaction transaction = new Transaction();
        network.init();
        network.notifyPeersOfNewTransaction(transaction, sourcePeer);
        verify(restTemplate).exchange(eq("http://alreadyKnownPeer:8080/api/addtransaction"), eq(HttpMethod.POST), ArgumentMatchers.<HttpEntity<Transaction>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<Object>>any());
    }

    @Test
    public void notifyPeerOfNewBlockShouldPropagateTheBlockToThePeers() {
        Peers peers = new Peers();
        peers.setPeers(Stream.of("alreadyKnownPeer:8080").collect(Collectors.toSet()));
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(peers));
        String sourcePeer = "sourcePeer:8080";
        Block block = new Block();
        network.init();
        network.notifyPeersOfNewBlock(block, sourcePeer);
        verify(restTemplate).exchange(eq("http://alreadyKnownPeer:8080/api/addblock"), eq(HttpMethod.POST), blockHttpEntityCaptor.capture(),
                ArgumentMatchers.<ParameterizedTypeReference<Object>>any());
        assertThat(blockHttpEntityCaptor.getValue().getHeaders().get("peer")).containsExactly("localhost:8080");
    }

    @Test
    public void retrieveBlockchainFromPeersShouldProduceBlockchainList() {
        Peers peers = new Peers();
        peers.setPeers(Stream.of("alreadyKnownPeer:8080").collect(Collectors.toSet()));
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(peers));
        Blockchain blockchain = new Blockchain();
        when(restTemplate.getForObject(eq("http://alreadyKnownPeer:8080/api/blockchain"), eq(Blockchain.class))).thenReturn(blockchain);
        network.init();
        List<Blockchain> blockchains = network.retrieveBlockchainsFromPeers();
        assertThat(blockchains).containsExactly(blockchain);
    }

    @Test
    public void brokenPeerShouldReduceThePeersListWhenRetrievingBlockchainFromPeers() {
        Peers peers = new Peers();
        peers.setPeers(Stream.of("alreadyKnownPeer:8080").collect(Collectors.toSet()));
        when(genericRepository.load(Peers.class)).thenReturn(Optional.of(peers));
        when(restTemplate.getForObject(eq("http://alreadyKnownPeer:8080/api/blockchain"), eq(Blockchain.class))).thenThrow(new ResourceAccessException("connection refused"));
        network.init();
        List<Blockchain> blockchains = network.retrieveBlockchainsFromPeers();
        verify(genericRepository).save(peersCaptor.capture());
        assertThat(network.getPeers()).doesNotContain("alreadyKnownPeer:8080");
        assertThat(peersCaptor.getValue().getPeers()).doesNotContain("alreadyKnownPeer:8080");
        assertThat(blockchains).isEmpty();
    }
}