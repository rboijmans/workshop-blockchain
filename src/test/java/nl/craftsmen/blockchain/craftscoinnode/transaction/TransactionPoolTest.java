package nl.craftsmen.blockchain.craftscoinnode.transaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionPoolTest {

    private TransactionPool transactionPool = new TransactionPool();
    private Transaction transaction;
    private Transaction transaction2;

    @Before
    public void setup() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        transactionPool = new TransactionPool();
        String transactionAsJson = "{\"id\":\"4c9e82bd-ef03-4b30-8b70-95c63aa5b188\", \"amount\":3,\"from\":\"michel\",\"to\":\"gerry\"}";

        String transactionAsJson2 = "{\"id\":\"4c9e82bd-ef03-4b30-8b70-95c63aa5b199\", \"amount\":4,\"from\":\"gerry\",\"to\":\"michel\"}";
        transaction = objectMapper.readValue(transactionAsJson, Transaction.class);
        transaction2 = objectMapper.readValue(transactionAsJson2, Transaction.class);

        transactionPool.addTransaction(transaction);
        transactionPool.addTransaction(transaction2);
    }

    @Test
    public void testGetTransactions() {
        assertThat(transactionPool.getAllTransactions()).containsExactlyInAnyOrder(transaction, transaction2);
    }

    @Test
    public void testRemoveSelectedTransactions() {
        transactionPool.clearTransactions(Collections.singletonList(transaction2));
        assertThat(transactionPool.getAllTransactions()).containsExactly(transaction);
    }

    @Test
    public void testRemoveAllTransactions() {
        transactionPool.clearTransactions();
        assertThat(transactionPool.getAllTransactions()).isEmpty();
    }

}