package nl.craftsmen.blockchain.craftscoinnode;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties("node")
public class CraftsCoinConfigurationProperties {

    @NotBlank
    private String bootstrapPeerHost;
    @NotNull
    private int bootstrapPeerPort;
    @NotBlank
    private String miningWalletId;

    private String ipAddress;

    public void setBootstrapPeerHost(String bootstrapPeerHost) {
        this.bootstrapPeerHost = bootstrapPeerHost;
    }

    public void setBootstrapPeerPort(int bootstrapPeerPort) {
        this.bootstrapPeerPort = bootstrapPeerPort;
    }

    public void setMiningWalletId(String miningWalletId) {
        this.miningWalletId = miningWalletId;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getBootstrapPeerHost() {
        return bootstrapPeerHost;
    }

    public int getBootstrapPeerPort() {
        return bootstrapPeerPort;
    }

    public String getMiningWalletId() {
        return miningWalletId;
    }

    public String getIpAddress() {
        return ipAddress;
    }
}
