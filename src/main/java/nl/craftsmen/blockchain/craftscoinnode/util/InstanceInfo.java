package nl.craftsmen.blockchain.craftscoinnode.util;

import nl.craftsmen.blockchain.craftscoinnode.CraftsCoinConfigurationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
public class InstanceInfo {

    private static final Logger LOGGER = LoggerFactory.getLogger(InstanceInfo.class);

    private String thisIpAdress;

    private int port;

    @Autowired
    public InstanceInfo(CraftsCoinConfigurationProperties configuration) {
        this.thisIpAdress = configuration.getIpAddress();
    }

    @PostConstruct
    public void init() {
        try {
            if (this.thisIpAdress == null) {
                LOGGER.info("property 'thisIpAddress' not set in application.properties, determining ip address...");
                this.thisIpAdress = InetAddress.getLocalHost().getHostAddress();
            }
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Gives the host and port the application is running on.
     *
     * @return the node address, for example: 192.168.1.250:8080
     */
    public String getNode() {
        return this.thisIpAdress + ":" + this.port;
    }

    public void setPort(int aPort) {
        this.port = aPort;
        LOGGER.info("node name of this node is {}", getNode());
    }

    public int getPort() {
        return port;
    }
}
