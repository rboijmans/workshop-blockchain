package nl.craftsmen.blockchain.craftscoinnode.network;

import nl.craftsmen.blockchain.craftscoinnode.CraftsCoinConfigurationProperties;
import nl.craftsmen.blockchain.craftscoinnode.blockchain.Block;
import nl.craftsmen.blockchain.craftscoinnode.blockchain.Blockchain;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;
import nl.craftsmen.blockchain.craftscoinnode.util.GenericRepository;
import nl.craftsmen.blockchain.craftscoinnode.util.InstanceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class Network {

    private static final Logger LOGGER = LoggerFactory.getLogger(Network.class);

    static final String API_REGISTERNODE = "/api/registernode";
    static final String API_ADDBLOCK = "/api/addblock";
    static final String API_ADDTRANSACTION = "/api/addtransaction";

    private final String bootstrapPeerHost;

    private final int bootstrapPeerPort;

    private Peers peers;

    private final RestTemplate restTemplate;

    private final InstanceInfo instanceInfo;

    private final GenericRepository genericRepository;

    @Autowired
    public Network(InstanceInfo instanceInfo, GenericRepository genericRepository, RestTemplate restTemplate, CraftsCoinConfigurationProperties configuration) {
        this.instanceInfo = instanceInfo;
        this.genericRepository = genericRepository;
        this.restTemplate = restTemplate;
        this.bootstrapPeerHost = configuration.getBootstrapPeerHost();
        this.bootstrapPeerPort = configuration.getBootstrapPeerPort();

    }

    public void init() {
        Optional<Peers> peers = genericRepository.load(Peers.class);
        if (!peers.isPresent()) {
            this.peers = new Peers();
            genericRepository.save(this.peers);
        } else {
            this.peers = peers.get();
        }
    }

    /**
     * Gets the list of known peers.
     *
     * @return the list of known peers.
     */
    public Set<String> getPeers() {
        return Collections.unmodifiableSet(peers.getPeers());
    }

    /**
     * Attempts the connect to the network. For this, the list of peers in the xxxxx-peers.json file is used. If no peers are known, the bootstrap peer is tried.
     */
    public void connectToNetwork() {
        if (peers.isEmpty()) {
            LOGGER.info("no peers known yet, trying to register this node to the bootstrap peer: {}", createHostEndpoint());
            try {
                if (InetAddress.getByName(bootstrapPeerHost).isLoopbackAddress() && (instanceInfo.getPort() == 0 || instanceInfo.getPort() == bootstrapPeerPort)) {
                    LOGGER.info("This instance is the bootstrap node. It will not register as a peer to itself.");
                } else {
                    registerThroughBootstrapNode();
                }
            } catch (UnknownHostException e) {
                throw new RuntimeException(e);
            }
        } else {
            LOGGER.info("registering this node to peers {}", peers);
            String newPeer = instanceInfo.getNode();
            propagateNewPeerAndCollectPeers(newPeer);

            genericRepository.save(peers);
        }
    }

    private void propagateNewPeerAndCollectPeers(String newPeer) {
        Set<String> newPeers = new HashSet<>();
        Set<String> peersToRemove = new HashSet<>();
        for (String peer : peersWithout(newPeer)) {
            ResponseEntity<Set<String>> listResponseEntity = post(peer, API_REGISTERNODE, newPeer, new ParameterizedTypeReference<Set<String>>() {
            }, peersToRemove);
            if (listResponseEntity != null) {
                Set<String> list = listResponseEntity.getBody();
                LOGGER.info("remote peer {} returned peer list: {}", peer, list);
                newPeers.addAll(list.stream().filter(e -> !e.equals(instanceInfo.getNode())).collect(Collectors.toSet()));
            }
        }
        peers.getPeers().removeAll(peersToRemove);
        peers.getPeers().addAll(newPeers);
    }

    private String createHostEndpoint() {
        return this.bootstrapPeerHost + ":" + bootstrapPeerPort;
    }

    private void registerThroughBootstrapNode() {
        ResponseEntity<Set<String>> listResponseEntity = post(createHostEndpoint(), API_REGISTERNODE, instanceInfo.getNode(), new ParameterizedTypeReference<Set<String>>() {
        }, new HashSet<>());
        if (listResponseEntity != null) {
            Set<String> list = listResponseEntity.getBody();
            LOGGER.info("received peer list: {}", list);
            LOGGER.info("adding peers to the list of known peers: {}", list);
            peers.getPeers().addAll(list);
            genericRepository.save(peers);
            LOGGER.info("the following peers are now known to this node: {}", peers);
        } else {
            LOGGER.warn("bootstrap node not found. I'm operating this blockchain on my own right now!");
        }
    }

    /**
     * Registers a new peer on this node.
     *
     * @param newPeer the new peer.
     * @return a list of known peers so the new peer can add them to its peers list.
     */
    Set<String> registerNewPeer(String newPeer) {
        LOGGER.info("a new peer has connected to the network: {}", newPeer);
        if (!peers.getPeers().contains(newPeer)) {
            LOGGER.info("peer {} is not previously known to this node. Peer registration will be forwarded to known peer: {}", newPeer, peers);
            propagateNewPeerAndCollectPeers(newPeer);

            peers.getPeers().add(newPeer);
            genericRepository.save(peers);
            LOGGER.info("adding node {} to the list of known peers.", newPeer);
        } else {
            LOGGER.info("peer {} is already known to this node. The node will not be forwarded to other peers.");
        }
        Set<String> peersForRemote = peersWithout(newPeer);
        peersForRemote.add(instanceInfo.getNode());
        LOGGER.info("returning the following peers to the peer that just registered itself: {}", peersForRemote);
        return peersForRemote;
    }


    /**
     * Notify other peers of a new transaction.
     *
     * @param transaction transaction
     * @param sourcePeer  the peer that sent the transsaction.
     */
    public void notifyPeersOfNewTransaction(Transaction transaction, String sourcePeer) {
        notifyPeersOf(API_ADDTRANSACTION, transaction, sourcePeer);
    }

    /**
     * Notify other peers of a new block.
     *
     * @param newBlock   the new block.
     * @param sourcePeer the peer that sent the new block.
     */
    public void notifyPeersOfNewBlock(Block newBlock, String sourcePeer) {
        notifyPeersOf(API_ADDBLOCK, newBlock, sourcePeer);
    }

    private <T> void notifyPeersOf(String operation, T t, String sourcePeer) {
        Set<String> peersWithoutSourcePeer = peersWithout(sourcePeer);
        LOGGER.info("notifying the following peers of new {} {}: {}", t.getClass().getSimpleName(), t, peersWithoutSourcePeer);
        Set<String> peersToRemove = new HashSet<>();
        for (String node : peersWithoutSourcePeer) {
            HttpEntity<T> entity = new HttpEntity<>(t, createHttpHeaders());
            post(node, operation, entity, new ParameterizedTypeReference<Object>() {
            }, peersToRemove);
        }
        peers.getPeers().removeAll(peersToRemove);
    }

    private Set<String> peersWithout(String sourcePeer) {
        return peers.getPeers().stream().filter(p -> !p.equals(sourcePeer)).collect(Collectors.toSet());
    }

    private HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("peer", instanceInfo.getNode());
        return httpHeaders;
    }

    /**
     * Gets the blockchain from all known peers.
     *
     * @return the list of blockchains.
     */
    public List<Blockchain> retrieveBlockchainsFromPeers() {
        List<Blockchain> blockchainList = new ArrayList<>();
        LOGGER.info("asking the following peers for their blockhain: {}", peers);
        Set<String> peersToRemove = new HashSet<>();
        for (String node : peers.getPeers()) {
            try {
                Blockchain otherChain = restTemplate.getForObject("http://" + node + "/api/blockchain", Blockchain.class);
                LOGGER.info("received blockchain from peer {}, adding it to the list.", node);
                blockchainList.add(otherChain);
            } catch (ResourceAccessException e) {
                LOGGER.info("could not connect to peer, remvong... {}", node);
                peersToRemove.add(node);
            }
            peers.getPeers().removeAll(peersToRemove);
        }
        genericRepository.save(peers);
        return blockchainList;
    }

    private <T> ResponseEntity<T> post(String peer, String action, Object object, ParameterizedTypeReference<T> typeRef, Set<String> peersToRemove) {
        try {
            HttpEntity<?> httpEntity;
            if (object instanceof HttpEntity) {
                httpEntity = (HttpEntity) object;
            } else {
                httpEntity = new HttpEntity<>(object);
            }
            return restTemplate.exchange("http://" + peer + action, HttpMethod.POST, httpEntity, typeRef);
        } catch (ResourceAccessException e) {
            LOGGER.warn("peer {} not found, removing from peer list.", peer);
            peersToRemove.add(peer);
        }
        return null;
    }


}
