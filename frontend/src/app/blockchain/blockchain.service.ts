import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

import { MessageService } from '../messages/message.service';
import { Block, Blockchain } from '../api';

@Injectable()
export class BlockchainService {
  constructor(private http: HttpClient, private messageService: MessageService) {}

  getBlockchain(): Observable<Blockchain> {
    this.messageService.log('Fetching blockchain...');

    return this.http.get<Blockchain>('/api/blockchain')
      .do(blockchain => {
        // todo: move sorting to backend?
        blockchain.chain = blockchain.chain.sort((block1: Block, block2: Block) => block2.index - block1.index);
      })
      .do(blockchain => this.messageService.log(`Got blockchain with ${blockchain.chain.length} block(s)`));
  }

  doMine(): Observable<Block> {
    this.messageService.log('Mining...');

    return this.http.post<Block>('/api/mine', {})
      .do(block => this.messageService.log(`Successfully mined block ${block.index} with ${block.transactions.length} transaction(s)`))
      .catch(() => {
        this.messageService.log(`ERROR while mining!`);

        return Observable.empty<Block>();
      });
  }
}
