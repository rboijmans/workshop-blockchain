import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { MessageService } from '../messages/message.service';

@Injectable()
export class PeersService {

  constructor(private http: HttpClient, private messageService: MessageService) {}

  getPeers(): Observable<string[]> {
    this.messageService.log('Fetching peers...');

    return this.http.get<string[]>('/api/peers')
      .do(peers => this.messageService.log(`Got ${peers.length} peer(s)`));
  }
}
