import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';

import { PeersService } from './peers.service';

@Component({
  selector: 'app-peers',
  templateUrl: './peers.component.html',
  styleUrls: ['./peers.component.scss']
})
export class PeersComponent implements OnInit, OnDestroy {
  peers: string[] = [];

  private subscription: Subscription;

  constructor(private peersService: PeersService) { }

  ngOnInit(): void {
    this.subscription = Observable.timer(0, 5000)
      .switchMap(() => this.peersService.getPeers())
      .subscribe(peers => this.peers = peers);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
